package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtNum1;
    private EditText txtNum2 ;
    private EditText txtResult;
    private Button btnSumar;
    private  Button  btnRestar;
    private  Button  btnMult;
    private  Button  btnDiv;
    private  Button  btnLimpiar;
    private  Button  btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEventos();
    }

    public void initComponents(){

        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResult = (EditText) findViewById(R.id.txtResult);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        setEventos();
    }

    public void setEventos(){

        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch( view.getId()){
            case  R.id.btnSuma:
                sumar();
                break;
            case  R.id.btnResta:
                Resta();
                break;
            case R.id.btnMult:
                Multiplicar();
                break;
            case R.id.btnDivi:
                Dividir();
                break;
            case R.id.btnLimpiar:
                Limpiar();
                break;
            case R.id.btnCerrar:
                Cerrar();
                break;
        }
    }

    public void sumar(){
        if (!txtNum1.getText().toString().isEmpty() && !txtNum2.getText().toString().isEmpty()) {
            op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
            op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
            txtResult.setText(String.valueOf(op.suma()));
        }
    }

    public void Resta(){
        if (!txtNum1.getText().toString().isEmpty() && !txtNum2.getText().toString().isEmpty()) {
            op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
            op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
            txtResult.setText(String.valueOf(op.resta()));
        }
    }
    public void Multiplicar(){
        if (!txtNum1.getText().toString().isEmpty() && !txtNum2.getText().toString().isEmpty()) {
            op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
            op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
            txtResult.setText(Float.toString(op.mult()));
        }
    }

    public void Dividir(){
        if (!txtNum1.getText().toString().isEmpty() && !txtNum2.getText().toString().isEmpty()) {
            op.setNum1(Float.parseFloat(txtNum1.getText().toString().trim()));
            op.setNum2(Float.parseFloat(txtNum2.getText().toString().trim()));
            txtResult.setText(Float.toString(op.div()));
        }
    }
    public void Limpiar(){
        txtNum1.setText("");
        txtNum2.setText("");
        txtResult.setText("");
    }

    public void Cerrar(){
        finish();
    }


}